package com.antonov.graphicstest;

import com.antonov.graphicstest.geometry.Ball;
import com.antonov.graphicstest.interfaces.Updatable;
import com.antonov.graphicstest.threading.UpdateThread;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyApplication extends Application implements Updatable {
    private static final int HEIGHT = 480;
    private static final int WIDTH = 640;
    private final List<Ball> balls;
    private final Random random;
    private final UpdateThread updateThread;
    private StackPane pane;
    private Canvas canvas;

    public MyApplication() {
        this.random = new Random(System.currentTimeMillis());
        this.balls = new ArrayList<>();
        this.updateThread = new UpdateThread();
        this.updateThread.start();
    }

    public static void main(String[] args) {
        launch("");
    }

    @Override
    public void start(Stage primaryStage) {
        this.canvas = new Canvas(WIDTH, HEIGHT);
        canvas.addEventFilter(MouseEvent.MOUSE_RELEASED, MyApplication.this::onMouseClicked);

        this.pane = new StackPane();
        pane.getChildren().add(canvas);
        Scene scene = new Scene(pane, WIDTH, HEIGHT);
        primaryStage.setTitle("Balls");
        primaryStage.setResizable(false);
        primaryStage.hide();
        primaryStage.setScene(scene);
        updateThread.registerOnUpdateThread(this, canvas.getGraphicsContext2D());
        primaryStage.show();
    }

    private void onMouseClicked(MouseEvent event) {
        balls.add(createBal(event.getX(), event.getY()));
    }

    private Ball createBal(double x, double y) {
        return new Ball(x, y, this.random.nextInt(80) + 20, 2, Color.BLUEVIOLET);
    }

    @Override
    public void update() {
        var iterator = balls.iterator();
        canvas.getGraphicsContext2D().clearRect(0, 0, WIDTH, HEIGHT);
        while (iterator.hasNext()) {
            Ball ball = iterator.next();
            ball.paint(canvas.getGraphicsContext2D());
            ball.setCy((ball.getCy() + ball.getRadius() / 10));
            if ((ball.getCy() - ball.getRadius()) > HEIGHT) {
                iterator.remove();
            }
        }
    }
}