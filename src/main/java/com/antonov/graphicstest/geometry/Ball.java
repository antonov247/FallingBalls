package com.antonov.graphicstest.geometry;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
public class Ball {
    private double cx;
    private double cy;
    private double radius;
    private double stroke;
    private Color color;

    public void paint(GraphicsContext graphics) {
        graphics.setStroke(color);
        graphics.strokeOval(cx - radius, cy - radius, radius * 2, radius * 2);
    }
}
