package com.antonov.graphicstest.threading;

import com.antonov.graphicstest.interfaces.Updatable;
import javafx.scene.canvas.GraphicsContext;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class UpdateThread extends Thread {
    private boolean isRunning;
    private final List<UpdatableUnit> updatables;

    public UpdateThread() {
        this.updatables = new ArrayList<>();
    }

    public void registerOnUpdateThread(Updatable updatable, GraphicsContext graphics) {
        this.updatables.add(new UpdatableUnit(updatable, graphics));
    }

    @Override
    public void run() {
        this.isRunning = true;
        while (isRunning) {
            for (UpdatableUnit updatableUnit : updatables) {
                updatableUnit.updatable().update();
            }
            try {
                Thread.sleep(17L);
            } catch (InterruptedException e) {
                this.isRunning = false;
            }
        }
        this.updatables.clear();
    }

    private record UpdatableUnit(Updatable updatable, GraphicsContext graphics) {
    }
}
