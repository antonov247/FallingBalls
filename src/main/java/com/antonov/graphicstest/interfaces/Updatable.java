package com.antonov.graphicstest.interfaces;

public interface Updatable {
    void update();
}
